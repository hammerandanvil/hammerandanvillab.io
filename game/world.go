package game

import "syscall/js"

type World struct {
	nodes js.Value
}

func NewWorld() *World {
	world := &World{}

	vis := js.Global().Get("vis")

	doc := js.Global().Get("document")
	visElement := doc.Call("getElementById", "vis")

	dataSet := vis.Get("DataSet")
	world.nodes = dataSet.New()

	node := js.Global().Get("Object").New()
	world.nodes.Call("add", node)

	edges := dataSet.New()
	datas := js.Global().Get("Object").New()
	datas.Set("nodes", world.nodes)
	datas.Set("edgess", edges)

	vis.Get("Network").New(
		visElement,
		datas,
		js.Global().Get("JSON").Call("parse", "{}"))

	return world
}
