package main

import (
	"fmt"
	"gitlab.com/hammerandanvil.gitlab.io/game"
	"runtime/debug"
)

func main() {
	defer func() {
		if r:= recover(); r != nil {
			fmt.Println(r)
			debug.PrintStack()
		}
	}()

	world := game.NewWorld()
	fmt.Println(world)
}